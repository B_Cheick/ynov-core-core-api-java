def call (){
    pipeline{
        agent any
        stages { 
            stage('Gradle Build'){
                tools{
                    jdk 'JDK17'
                }
                steps {
                    sh 'chmod +x gradlew'
                    sh './gradlew clean build'
                }
            }
        }
    }
}
